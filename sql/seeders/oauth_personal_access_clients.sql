/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

--
-- Seed `oauth_personal_access_clients` table
--
INSERT INTO `oauth_personal_access_clients` (
    `id`,
    `client_id`,
    `created_at`,
    `updated_at`
) VALUES
(1, 1, "2022-04-01 19:00:43", "2022-04-01 19:00:43");

/*!40101 SET character_set_client = @saved_cs_client */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;