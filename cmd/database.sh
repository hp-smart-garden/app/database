#!/bin/bash

# --------------------------------------------------------------
# Create database script
# @author Hervé Perchec <herve.perchec@gmail.com>
# --------------------------------------------------------------

# Get entire path of the script directory
SCRIPT_DIR=$(dirname "${BASH_SOURCE[0]}")
cd $SCRIPT_DIR
SCRIPT_DIR=$PWD

# Get sql directory path
SQL_DIR=$SCRIPT_DIR/../sql

# Get config (DATABASE_NAME var)
source ../config

# Defines if it must force database re-creating (default: 0)
FORCE_INIT=0
# By default, user must validate manually the operation (in case of FORCE_INIT=1)
PROMPT_FORCE_INIT=1

# Defines the verbosity level
# Can be 'NONE', 'LOW', 'MEDIUM' or 'HIGH', default: 'HIGH'
VERBOSE_LEVEL=HIGH

# Init exit code, default: 0 (Success)
EXIT_CODE=0

# Show header function
show_header() {
    echo -e "\e[36m-------------------------------------------------------------\e[0m"
    echo -e "\e[36mCreate database script\e[0m"
    echo -e "\e[36m@author Hervé Perchec <herve.perchec@gmail.com>\e[0m"
    echo -e "\e[36m-------------------------------------------------------------\e[0m"
    echo
}

# Show help function
show_help() {
    echo -e "Usage: database.sh \e[33m[OPTIONS]\e[0m"
    echo
    echo -e "\e[33mOPTIONS:\e[0m"
    echo
    echo -e "  \e[33m-h\e[0m         Show this help."
    echo -e "  \e[33m-f\e[0m         Force script to drop and re-create database."
    echo -e "  \e[33m-y\e[0m         Auto-confirm (in case of -f option provided)."
    echo -e "  \e[33m-v\e[0m         Defines the verbose level. Can be 'NONE', 'LOW', 'MEDIUM' or 'HIGH' (Default: 'HIGH')."
    echo
}

# Quit function
quit() {
    exit $EXIT_CODE
}

# --------------------------------------------------------------
# END INIT
# --------------------------------------------------------------

# Get options
# -h : To show command help
# -f : To force script even if database is already created
# -y : Auto-confirm when FORCE_INIT=1
# -v : To define verbose level
while getopts "hfyv:" opt
do
    case $opt in
        f)
            FORCE_INIT=1
            ;;
        y)
            PROMPT_FORCE_INIT=0
            ;;
        v) 
            case $OPTARG in
                "NONE") VERBOSE_LEVEL=${OPTARG} ;;
                "LOW") VERBOSE_LEVEL=${OPTARG} ;;
                "MEDIUM") VERBOSE_LEVEL=${OPTARG} ;;
                "HIGH") VERBOSE_LEVEL=${OPTARG} ;;
                *) 
                    show_header
                    echo -e "\e[31mInvalid value for -v option\e[0m"
                    echo
                    show_help
                    EXIT_CODE=1 # Exit code: 1 (Error)
                    quit
                    ;;
            esac
            ;;
        h) 
            show_help
            quit
            ;;
        *)
            echo
            show_help
            EXIT_CODE=1 # Exit code: 1 (Error)
            quit
            ;;
    esac
done

# --------------------------------------------------------------
# END SCRIPT OPTIONS PARSING
# --------------------------------------------------------------

# Call show_header
if [ $VERBOSE_LEVEL == "HIGH" ]; then
    show_header
fi

# --------------------------------------------------------------
# MAIN FUNCTION
# --------------------------------------------------------------

main() {

    #
    # 1 - Check if DATABASE_NAME var is defined
    #

    # DATABASE_NAME cannot be empty
    if [ -z ${DATABASE_NAME} ]; then
        echo -e "\e[31mError: You need to define DATABASE_NAME in the ./config file\e[0m"
        echo
        EXIT_CODE=1 # Exit code: 1 (Error)
        quit
    fi


    #
    # 2 - Check if database is already created
    #

    if mysql "$DATABASE_NAME" -e exit > /dev/null 2>&1; then

        # Display message if VERBOSE_LEVEL !== "NONE"
        if [ $VERBOSE_LEVEL != "NONE" ]; then
            echo -e "\e[33mDatabase '$DATABASE_NAME' is already created.\e[0m"
            echo
        fi

        # Check if -f option is provided and -y NOT provided
        if [ $FORCE_INIT -eq 1 ]; then
            # Check if -y option is not provided
            if [ $PROMPT_FORCE_INIT -eq 1 ]; then
                echo -e "\e[33mIt will re-create the entire database. Are you sure?\e[0m"
                echo
                choices=("Continue" "Exit")
                select choice in "${choices[@]}"
                do
                    case $choice in
                        "Continue")
                            break
                            ;;
                        "Exit")
                            echo -e "\e[33mScript aborted.\e[0m"
                            echo
                            EXIT_CODE=1 # Exit code: 1 (Error)
                            quit
                            break
                            ;;
                        *) 
                            echo -e "\e[31mInvalid choice $REPLY\e[0m"
                            ;;
                    esac
                done
                echo
            fi
        else
            EXIT_CODE=1 # Exit code: 1 (Error)
            quit
        fi

    fi

    #
    # 3 - If FORCE_INIT, drop database before re-creating
    #

    if [ $FORCE_INIT -eq 1 ]; then
        if [ $VERBOSE_LEVEL == "MEDIUM" ] || [ $VERBOSE_LEVEL == "HIGH" ]; then
            echo -ne "\e[36m- Drop $DATABASE_NAME database: \e[0m"
        fi
        # Drop database
        mysql -e "DROP DATABASE $DATABASE_NAME;"
        if [ $VERBOSE_LEVEL == "MEDIUM" ] || [ $VERBOSE_LEVEL == "HIGH" ]; then
            echo -e "\e[32mSuccess\e[0m"
            echo
        fi
    fi

    #
    # 4 - Create database
    #

    if [ $VERBOSE_LEVEL == "MEDIUM" ] || [ $VERBOSE_LEVEL == "HIGH" ]; then
        echo -ne "\e[36m- Create $DATABASE_NAME database: \e[0m"
    fi
    # Get database.sql file content
    _SQL=$(cat "$SQL_DIR/database.sql")
    # Replace __DATABASE_NAME__
    _SQL=$(echo "$_SQL" | sed 's/__DATABASE_NAME__/'"$DATABASE_NAME"'/')
    # Execute SQL
    mysql <<< $_SQL
    if [ $VERBOSE_LEVEL == "MEDIUM" ] || [ $VERBOSE_LEVEL == "HIGH" ]; then
        echo -e "\e[32mSuccess\e[0m"
        echo
    fi

    #
    # 5 - Create migrations table
    #

    if [ $VERBOSE_LEVEL == "MEDIUM" ] || [ $VERBOSE_LEVEL == "HIGH" ]; then
        echo -ne "\e[36m- Create 'migrations' table: \e[0m"
    fi
    mysql -D $DATABASE_NAME < $SQL_DIR/migrations_table.sql
    if [ $VERBOSE_LEVEL == "MEDIUM" ] || [ $VERBOSE_LEVEL == "HIGH" ]; then
        echo -e "\e[32mSuccess\e[0m"
        echo
    fi

    #
    # End - Success message
    #

    if [ $VERBOSE_LEVEL != "NONE" ]; then
        echo -e "\e[32mCreate database script successfully executed.\e[0m"
        echo
    fi

}

# --------------------------------------------------------------

# Call main()
main

# Exit with success
quit