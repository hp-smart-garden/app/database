/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

--
-- Seed `errors` table
--
INSERT INTO `errors` (
    `error_id`,
    `fr_message`,
    `en_message`
) VALUES
("e0000", "Erreur inconnue", "Unknown error"),
("e0010", "Une erreur est survenue lors du traitement de la requête. Veuillez ré-essayer.", "An error has occured during the request. Please try again."),
("e0010-400", "Requête invalide", "Bad Request"),
("e0010-401", "Authentification requise", "Unauthenticated"),
("e0010-402", "Paiement requis", "Payment Required"),
("e0010-403", "Non autorisé", "Forbidden"),
("e0010-404", "Introuvable", "Not found"),
("e0010-405", "Méthode non autorisée", "Method Not Allowed"),
("e0010-409", "Conflit", "Conflict"),
("e0010-422", "Entité fournie invalide ou incomplète", "Unprocessable entity"),
("e0010-500", "Erreur interne du serveur", "Internal Server Error"),
("e0010-501", "Non implémenté", "Not Implemented"),
("e0010-502", "Mauvaise passerelle / Erreur proxy", "Bad Gateway / Proxy Error"),
("e0010-503", "Service non disponible", "Service Unavailable"),
("e0010-504", "Temps d'attente de réponse dépassé", "Gateway Time-out"),
("e0011", "Une erreur est survenue lors du chargement des données. Veuillez ré-essayer.", "An error has occured during the data loading. Please try again."),
("e0012", "Vous n'êtes pas autorisé à effectuer cette action.", "You don't have permission to do this action"),
("e0013", "Vous n'êtes pas autorisé à effectuer l'action suivante: {action}", "You don't have permission to do the following action: {action}"),
("e0015", "Entité introuvable", "Not found entity"),
("e0020", "Identifiants incorrects", "Bad credentials"),
("e0021", "Session invalide ou expirée. Veuillez vous reconnecter.", "Invalid or expired session. Please try to log again."),
("e0022", "Le mot de passe saisi est incorrect", "Invalid password given"),
("e0023", "Vous devez être le propriétaire de la ressource pour effectuer cette action.", "You must be the owner of the resource to do this action."),
("e0030", "Le mot de passe de respecte pas les règles de sécurité. Veuillez ré-essayer.", "Password does not match security policy. Try again."),
("e0031", "L'email que vous avez saisi est déjà utilisée.", "Email has already been taken."),
("e0032", "L'email saisi ne respecte pas le format requis.", "Email doesn't match required format"),
("e0040", "Le lien de vérification d'adresse email n'est plus valide. Veuillez ré-essayer.", "The email verification link is not valid. Please try again."),
("e0050", "Un champ du formulaire n'est pas valide", "A form field is invalid"),
("e0051", "Ce champ n'est pas valide", "This field is invalid"),
("e0052", "Le champ {field} n'est pas valide", "The field {field} is invalid");
-- ("e0101", "Une description en français", "An english description")

/*!40101 SET character_set_client = @saved_cs_client */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;