/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

--
-- Seed `users` table
--
INSERT INTO `users` (
    `id`,
    `username`,
    `password`,
    `thumbnail`,
    `role`,
    `remember_token`,
    `created_at`,
    `updated_at`
) VALUES
(
    1,
    "admin",
    "$2y$10$jdRbFXlpl2gRDFHAX.moIOtZUXmBHDMttjPO.WzuviJajYKYLuDIG", -- 'admin'
    NULL,
    "admin",
    NULL,
    "2022-04-01 19:00:44",
    "2022-04-01 19:00:44"
),
(
    2,
    "test",
    "$2y$10$VpH0QUfK3ZYlzfBaLEF8MO6gjv83smGalXVEdW/P9SxbiekoQks9.", -- 'test'
    NULL,
    "user",
    NULL,
    "2022-04-01 19:00:44",
    "2022-04-01 19:00:44"
);

/*!40101 SET character_set_client = @saved_cs_client */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;