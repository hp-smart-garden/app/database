--
-- Migration: create_oauth_refresh_tokens_table
--

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;

--
-- Description:
--
-- Create oauth_refresh_tokens table
--

DELIMITER //

--
-- UP
--

CREATE PROCEDURE UP ()

BEGIN

  -- Create `oauth_refresh_tokens` table
  CREATE TABLE `oauth_refresh_tokens` (

    -- Fields
    `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
    `access_token_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
    `revoked` tinyint(1) NOT NULL,
    `expires_at` datetime DEFAULT NULL,

    -- Keys
    PRIMARY KEY (`id`),
    KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)

  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

END//

--
-- DOWN
--

CREATE PROCEDURE DOWN ()

BEGIN

  -- Drop oauth_refresh_tokens table
  DROP TABLE `oauth_refresh_tokens`;

END//

DELIMITER ;

--

-- ! Don't remove these lines !
SET @query = CONCAT('CALL ', @PROC, '()');
PREPARE stmt FROM @query; EXECUTE stmt;
-- ! Don't remove these lines !

-- Reset global variables here

/*!40101 SET character_set_client = @saved_cs_client */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;