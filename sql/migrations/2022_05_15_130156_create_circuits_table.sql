--
-- Migration: create_circuits_table
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;

--
-- Description:
--
-- <enter the description>
--

DELIMITER //

--
-- UP
--

CREATE PROCEDURE UP ()

BEGIN

    -- Create `circuits` table
    CREATE TABLE `circuits` (

        -- Fields
        `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        `channel` bigint(20) unsigned NOT NULL COMMENT 'Channel number',
        `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Circuit name',
        `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Circuit description',
        `type` enum('valve','water_inlet') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'valve' COMMENT 'Circuit type (''valve'' or ''water_inlet'')',
        `color` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Circuit color (format: #XXXXXX). Length: 7 chars',

        -- Keys
        PRIMARY KEY (`id`),
        UNIQUE KEY `circuit_channel_unique` (`id`, `channel`)

  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

END//

--
-- DOWN
--

CREATE PROCEDURE DOWN ()

BEGIN

    DROP TABLE `circuits`;

END//

DELIMITER ;

--

-- ! Don't remove these lines !
SET @query = CONCAT('CALL ', @PROC, '()');
PREPARE stmt FROM @query; EXECUTE stmt;
-- ! Don't remove these lines !

-- Reset global variables here

/*!40101 SET character_set_client = @saved_cs_client */;