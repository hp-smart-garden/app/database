--
-- Migration: create_circuit_status_history_table
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;

--
-- Description:
--
-- <enter the description>
--

DELIMITER //

--
-- UP
--

CREATE PROCEDURE UP ()

BEGIN

  -- Create `circuit_status_history` table
  CREATE TABLE `circuit_status_history` (

    -- Fields
    `circuit_id` bigint(20) unsigned NOT NULL,
    `status` tinyint(1) NOT NULL,
    `created_at` datetime DEFAULT NULL,

    -- Keys
    PRIMARY KEY (`circuit_id`, `created_at`),
    KEY `circuits_id_foreign` (`circuit_id`),

    -- Foreign keys
    CONSTRAINT `circuits_id_foreign` FOREIGN KEY (`circuit_id`) REFERENCES `circuits` (`id`) ON DELETE CASCADE

  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

END//

--
-- DOWN
--

CREATE PROCEDURE DOWN ()

BEGIN

  -- Drop table `circuit_status_history`
  DROP TABLE `circuit_status_history`;

END//

DELIMITER ;

--

-- ! Don't remove these lines !
SET @query = CONCAT('CALL ', @PROC, '()');
PREPARE stmt FROM @query; EXECUTE stmt;
-- ! Don't remove these lines !

-- Reset global variables here

/*!40101 SET character_set_client = @saved_cs_client */;