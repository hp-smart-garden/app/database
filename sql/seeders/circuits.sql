/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;

--
-- Seed `circuits` table
--
INSERT INTO `circuits` (
    `id`,
    `channel`,
    `name`,
    `description`,
    `type`,
    `color`
) 
VALUES
(1, 1, 'Circuit 1', NULL, 'water_inlet', '#FFAA00'),
(2, 2, 'Circuit 2', NULL, 'valve', '#FFAA00'),
(3, 3, 'Circuit 3', NULL, 'valve', '#FFAA00');

/*!40101 SET character_set_client = @saved_cs_client */;