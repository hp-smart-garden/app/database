/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

--
-- Seed `oauth_clients` table
--
INSERT INTO `oauth_clients` (
    `id`,
    `user_id`,
    `name`,
    `secret`,
    `provider`,
    `redirect`,
    `personal_access_client`,
    `password_client`,
    `revoked`,
    `created_at`,
    `updated_at`
) VALUES
(1, NULL, "App Personal Access Client", "Da20ZKKMS8p1s1cqPR3H4Cys1OIoUqpH4Ue6j3W6", NULL, "http://localhost", 1, 0, 0, "2022-04-01 19:00:43", "2022-04-01 19:00:43"),
(2, NULL, "App Password Grant Client", "m4WUbg5qZDUwhCvTRRTn5iNTW1rOlhPSKYIoJfgB", "users", "http://localhost", 0, 1, 0, "2022-04-01 19:00:43", "2022-04-01 19:00:43");

/*!40101 SET character_set_client = @saved_cs_client */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;