#!/bin/bash

# --------------------------------------------------------------
# Run migrations script
# @author Hervé Perchec <herve.perchec@gmail.com>
# --------------------------------------------------------------

# Get entire path of the script directory
SCRIPT_DIR=$(dirname "${BASH_SOURCE[0]}")
cd $SCRIPT_DIR
SCRIPT_DIR=$PWD

# Get sql directory path
SQL_DIR=$SCRIPT_DIR/../sql

# Get config (DATABASE_NAME var)
source ../config

# Get sql/migrations directory path
MIGRATIONS_DIR=$SQL_DIR/migrations

# Defines if migrations must be rollbacked
ROLLBACK=0
# Loop on files in reverse order (empty string by default)
REVERSE=

# Define if there are some migrations to execute
NOTHING_TO_MIGRATE=1

# Defines the verbosity level
# Can be 'NONE', 'LOW', 'MEDIUM' or 'HIGH', default: 'HIGH'
VERBOSE_LEVEL=HIGH

# Init exit code, default: 0 (Success)
EXIT_CODE=0

# Show header function
show_header() {
    echo -e "\e[36m-------------------------------------------------------------\e[0m"
    echo -e "\e[36mRun migrations script\e[0m"
    echo -e "\e[36m@author Hervé Perchec <herve.perchec@gmail.com>\e[0m"
    echo -e "\e[36m-------------------------------------------------------------\e[0m"
    echo
}

# Show help function
show_help() {
    echo -e "Usage: migrate.sh \e[36m[<name>]\e[0m \e[33m[OPTIONS]\e[0m"
    echo
    echo -e "\e[36mARGS:\e[0m"
    echo
    echo -e "  \e[36m<name>\e[0m     Target migration name (optional)."
    echo
    echo -e "\e[33mOPTIONS:\e[0m"
    echo
    echo -e "  \e[33m-h\e[0m         Show this help."
    echo -e "  \e[33m-v\e[0m         Defines the verbose level. Can be 'NONE', 'LOW', 'MEDIUM' or 'HIGH' (Default: 'HIGH')."
    echo -e "  \e[33m-R\e[0m         Rollback: defines if migrations must be rollbacked."
    echo
}

# Migrate function
migrate() {

    # Get first arg -> migration filename
    local _filename=$1
    # Up or rollback ('UP' or 'DOWN')
    local _status=$2
    # Insert below __MIGRATION_CONTENT__
    local _SQL=$(sed -e '/__MIGRATION_CONTENT__/r'$MIGRATIONS_DIR/$_filename $MIGRATIONS_DIR/template/migrate.sql)
    # Remove __MIGRATION_CONTENT__
    local _SQL=$(echo "$_SQL" | sed 's/__MIGRATION_CONTENT__//')
    # Replace __DATABASE_NAME__
    local _SQL=$(echo "$_SQL" | sed 's/__DATABASE_NAME__/'"$DATABASE_NAME"'/')
    # Replace __PROC__
    local _SQL=$(echo "$_SQL" | sed 's/__PROC__/'"$_status"'/')
    # Write full SQL in temporary file
    echo "$_SQL" > $MIGRATIONS_DIR/tmp/$_filename.tmp
    # Remove comments
    sed -i '/^ *--.*/d' $MIGRATIONS_DIR/tmp/$_filename.tmp
    # Replace CRLF by LF
    sed -i 's/\r$//' $MIGRATIONS_DIR/tmp/$_filename.tmp
    # Remove blank lines
    sed -i '/^$/d' $MIGRATIONS_DIR/tmp/$_filename.tmp
    # Add a comment that contains execution timestamp
    local _current_timestamp=$(date +'%Y/%m/%d %H:%M:%S')
    sed -i '1 i -- Executed: '"$_current_timestamp"' ('"$_status"')' $MIGRATIONS_DIR/tmp/$_filename.tmp
    # Call the SQL script
    mysql <<< $_SQL

}

# Quit function
quit() {
    exit $EXIT_CODE
}

# --------------------------------------------------------------
# END INIT
# --------------------------------------------------------------

# Loop on passed arguments
#
# Solution found on stackoverflow to not give matter of OPTIONS/ARG position
# It can be :
#   - migrate.sh OPTIONS ARG
#   - migrate.sh ARG OPTIONS
#   - migrate.sh OPTION ARG OPTION
#   - etc...
# https://stackoverflow.com/questions/11742996/is-mixing-getopts-with-positional-parameters-possible
while [ $# -gt 0 ]
do
    unset OPTIND
    unset OPTARG
    # Get options
    # -h : To show command help
    # -R : To rollback migrations
    # -v : To define verbose level
    while getopts "hRv:" opt
    do
        case $opt in
            R) 
                ROLLBACK=1
                REVERSE="-r" # -r option for ls command (see below)
                ;;
            v) 
                case $OPTARG in
                    "NONE") VERBOSE_LEVEL=${OPTARG} ;;
                    "LOW") VERBOSE_LEVEL=${OPTARG} ;;
                    "MEDIUM") VERBOSE_LEVEL=${OPTARG} ;;
                    "HIGH") VERBOSE_LEVEL=${OPTARG} ;;
                    *) 
                        show_header
                        echo -e "\e[31mInvalid value for -v option\e[0m"
                        echo
                        show_help
                        EXIT_CODE=1 # Exit code: 1 (Error)
                        quit
                        ;;
                esac
                ;;
            h) 
                show_help
                quit
                ;;
            *)
                echo
                show_help
                EXIT_CODE=1 # Exit code: 1 (Error)
                quit
                ;;
        esac
    done
    shift $((OPTIND-1))
    ARGS="${ARGS:+${ARGS} }${1}"
    shift
done

# Build array of args
ARGS=($(echo $ARGS))
# Get first arg -> target migration name (optional)
TARGET_MIGRATION=${ARGS[0]}

# --------------------------------------------------------------
# END SCRIPT OPTIONS PARSING
# --------------------------------------------------------------

# Call show_header
if [ $VERBOSE_LEVEL == "HIGH" ]; then
    show_header
fi

# --------------------------------------------------------------
# MAIN FUNCTION
# --------------------------------------------------------------

main() {

    #
    # 1 - Get migrations from database table
    #

    # Select only migration name, without column names (-N), in a readable format (-s)
    MIGRATIONS_TABLE_RESULT=$(mysql -D $DATABASE_NAME -sNe "SELECT migration, batch FROM migrations;")

    #
    # 2 - Loop on sql files to migrate
    #

    # Defines if target migration is found
    TARGET_MIGRATION_FOUND=0 # False by default

    for filepath in $(ls $REVERSE $MIGRATIONS_DIR/*.sql); do

        # Filename
        filename=$(basename $filepath)
        # Migration name
        migration_name=$(basename $filepath .sql) # Remove ".sql" from file name
        # Get table entry from mysql result
        table_entry=$(echo "$MIGRATIONS_TABLE_RESULT" | grep "$migration_name")

        # Check if TARGET_MIGRATION is defined
        if [[ ! -z ${TARGET_MIGRATION} ]]; then
            if [ $TARGET_MIGRATION == $migration_name ]; then
                TARGET_MIGRATION_FOUND=1
            else
                continue
            fi
        fi

        # If ROLLBACK
        if [ $ROLLBACK -eq 1 ]; then

            UP_OR_DOWN=DOWN
            TO_BATCH=0

            # Set migration_status to 0 -> NEED TO BE MIGRATED by default
            migration_status=0

            # If migration already exists in migrations table
            if [[ ! -z ${table_entry} ]]; then
                # Get "batch" field value
                migration_batch=$(echo $table_entry | cut -d " " -f2) # Split string using "space" delimiter and get SECOND word
                # If batch = 0 -> already rollbacked
                if [ $migration_batch -eq 0 ]; then
                    # Set migration_status to 1 -> ALREADY ROLLBACKED
                    migration_status=1
                fi
            fi

            if [ $VERBOSE_LEVEL == "MEDIUM" ] || [ $VERBOSE_LEVEL == "HIGH" ]; then
                echo -ne "- Rollback \e[36m'${migration_name:18}'\e[0m: "
            fi

        # Else -> migrate (up)
        else

            UP_OR_DOWN=UP
            TO_BATCH=1

            # Set migration_status to 0 -> NEED TO BE MIGRATED by default
            migration_status=0

            # If migration already exists in migrations table
            if [[ ! -z ${table_entry} ]]; then
                # Get "batch" field value
                migration_batch=$(echo $table_entry | cut -d " " -f2) # Split string using "space" delimiter and get SECOND word
                # If batch = 1 -> already executed
                if [ $migration_batch -eq 1 ]; then
                    # Set migration_status to 1 -> ALREADY MIGRATED
                    migration_status=1
                fi
            fi

            if [ $VERBOSE_LEVEL == "MEDIUM" ] || [ $VERBOSE_LEVEL == "HIGH" ]; then
                echo -ne "- Migrate \e[36m'${migration_name:18}'\e[0m: "
            fi

        fi

        # If migration needs to be executed
        if [ $migration_status -eq 0 ]; then
            NOTHING_TO_MIGRATE=0
            # Migrate (Run sql script)
            migrate $filename $UP_OR_DOWN > /dev/null 2>&1 # Don't show any output
            # If mysql cmd exit code is error -> abort
            if [ $? -ne 0 ]; then
                if [ $VERBOSE_LEVEL == "MEDIUM" ] || [ $VERBOSE_LEVEL == "HIGH" ]; then
                    echo -e "\e[31mFailed\e[0m"
                fi
                migrate $filename $UP_OR_DOWN
                echo
                EXIT_CODE=1
                quit
            else
                if [ $VERBOSE_LEVEL == "MEDIUM" ] || [ $VERBOSE_LEVEL == "HIGH" ]; then
                    echo -e "\e[32mSuccess\e[0m"
                fi
                # Insert or update table entry
                if [[ ! -z ${table_entry} ]]; then
                    mysql -D $DATABASE_NAME -e "UPDATE migrations SET batch=$TO_BATCH WHERE migration='$migration_name';" > /dev/null 2>&1 # Don't show any output
                else
                    mysql -D $DATABASE_NAME -e "INSERT INTO migrations (migration, batch) VALUES ('$migration_name', $TO_BATCH);" > /dev/null 2>&1 # Don't show any output
                fi
            fi
        else
            if [ $VERBOSE_LEVEL == "MEDIUM" ] || [ $VERBOSE_LEVEL == "HIGH" ]; then
                if [ $ROLLBACK -eq 1 ]; then 
                    echo -e "\e[33malready rollbacked\e[0m"
                else
                    echo -e "\e[33malready executed\e[0m"
                fi
            fi
        fi

    done

    # If TARGET_MIGRATION is defined
    if [[ ! -z ${TARGET_MIGRATION} ]]; then
        # If not found -> return error
        if [ $TARGET_MIGRATION_FOUND -eq 0 ]; then
            echo -e "\e[31mERROR: Migration '$TARGET_MIGRATION' does not exist...\e[0m"
            echo
            EXIT_CODE=1
            quit
        fi
    fi

    #
    # End - Success message
    #

    if [ $VERBOSE_LEVEL != "NONE" ]; then
        echo
        if [ $NOTHING_TO_MIGRATE -eq 0 ]; then
            echo -e "\e[32mMigrations successfully executed.\e[0m"
        else
            if [ $ROLLBACK -eq 1 ]; then
                echo -e "\e[33mNothing to rollback.\e[0m"
            else
                echo -e "\e[33mNothing to migrate.\e[0m"
            fi
        fi
        echo
    fi

}

# --------------------------------------------------------------

# Call main()
main

# Exit with success
quit
