-------------------------------------------------------------
-- ! THIS COMMENT BLOCK WILL BE REMOVED !
-- MIGRATION TEMPLATE
-- @author Hervé Perchec <herve.perchec@gmail.com>
-------------------------------------------------------------
--
-- Migration: __MIGRATION_NAME__
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;

--
-- Description:
--
-- <enter the description>
--

DELIMITER //

--
-- UP
--

CREATE PROCEDURE UP ()

BEGIN

    ALTER TABLE `__TABLE_NAME__`

        -- Do stuff here...

    ;

END//

--
-- DOWN
--

CREATE PROCEDURE DOWN ()

BEGIN

    ALTER TABLE `__TABLE_NAME__`

        -- Do stuff here...

    ;

END//

DELIMITER ;

--

-- ! Don't remove these lines !
SET @query = CONCAT('CALL ', @PROC, '()');
PREPARE stmt FROM @query; EXECUTE stmt;
-- ! Don't remove these lines !

-- Reset global variables here

/*!40101 SET character_set_client = @saved_cs_client */;